//
//  DeviceMotion.m
//  PeerAttitude
//
//  Created by Justin on 2018-11-07.
//  Copyright © 2018 Justin. All rights reserved.
//

/*
 
 CS4768 Assignment 3
 
 Justin Delaney
 201222684
 
 
 */

#import <Foundation/Foundation.h>
#import "DeviceMotion.h"
#import <CoreMotion/CMMotionManager.h>
#import <UIKit/UIKit.h>
#import <MultipeerConnectivity/MultipeerConnectivity.h>

@interface DeviceMotion() {
    BOOL connected;
}

// Properties for reading and display motion manager data
@property (nonatomic, strong) CMMotionManager *motionManager;
@property (nonatomic, strong) NSTimer *timer;
@property (weak, nonatomic) IBOutlet UILabel *acc_X;
@property (weak, nonatomic) IBOutlet UILabel *acc_Y;
@property (weak, nonatomic) IBOutlet UILabel *acc_Z;
@property (weak, nonatomic) IBOutlet UILabel *gyX;
@property (weak, nonatomic) IBOutlet UILabel *gyY;
@property (weak, nonatomic) IBOutlet UILabel *gyZ;

// Properties for handling peer network
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIButton *disconnectButton;
@property (weak, nonatomic) IBOutlet UIButton *browseButton;

@property (strong, nonatomic) MCSession *session;
@property (strong, nonatomic) MCAdvertiserAssistant *assistant;
@property (strong, nonatomic) MCBrowserViewController *browserVC;

- (IBAction)browseButtonTapped:(UIButton *)sender;
- (IBAction)disconnectButtonTapped:(UIButton *)sender;
- (IBAction)StartButtonTapped:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *StartButton;

- (void)setUIToNotConnectedState;
- (void)setUIToConnectedState;

@end

@implementation DeviceMotion

float avgX, avgY, avgZ;

- (void)updateLabels:(float)roll Pitch:(float)pitch Yaw:(float)yaw accX:(float)accX accY:(float)accY accZ:(float)accZ
{
    float alpha = 0.1;
    avgX = alpha*accX + (1-alpha)*avgX; // Apply low pass filter
    avgY = alpha*accY + (1-alpha)*avgY;
    avgZ = alpha*accZ + (1-alpha)*avgZ;
    
    NSString* formattedX = [NSString stringWithFormat:@"%.03f", avgX]; // Format values to 3 decimal places
    NSString* formattedY = [NSString stringWithFormat:@"%.03f", avgY];
    NSString* formattedZ = [NSString stringWithFormat:@"%.03f", avgZ];
    
    NSString* formattedX_G = [NSString stringWithFormat:@"%.03f", roll]; // Format values to 3 decimal places
    NSString* formattedY_G = [NSString stringWithFormat:@"%.03f", pitch];
    NSString* formattedZ_G = [NSString stringWithFormat:@"%.03f", yaw];
    
    
    _acc_X.text = formattedX; // Update acceleration labels
    _acc_Y.text = formattedY;
    _acc_Z.text = formattedZ;
    
    _gyX.text = formattedX_G; // Update gyro labels 
    _gyY.text = formattedY_G;
    _gyZ.text = formattedZ_G;
    
    
}

-(void)viewDidLoad {
    
    // Prepare session
    MCPeerID *myPeerID = [[MCPeerID alloc] initWithDisplayName:[[UIDevice currentDevice] name]];
    self.session = [[MCSession alloc] initWithPeer:myPeerID];
    self.session.delegate = self;
    
    // Start advertising
    self.assistant = [[MCAdvertiserAssistant alloc] initWithServiceType:SERVICE_TYPE discoveryInfo:nil session:self.session];
    [self.assistant start];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(startMonitoringMotion) userInfo:nil repeats:YES];
    
    self.motionManager = [[CMMotionManager alloc] init]; // Create instance of motion manager
    self.motionManager.deviceMotionUpdateInterval = 0.05; // Update interval of 60hz
    
    [self.motionManager startDeviceMotionUpdatesUsingReferenceFrame: CMAttitudeReferenceFrameXArbitraryCorrectedZVertical];
}

-(void) viewDidAppear:(BOOL)animated
{
    if (connected)
        [self setUIToConnectedState];
    else
        [self setUIToNotConnectedState];
    
}

- (IBAction)browseButtonTapped:(UIButton *)sender { // Start showing devices
    self.browserVC = [[MCBrowserViewController alloc] initWithServiceType:SERVICE_TYPE session:self.session];
    self.browserVC.delegate = self;
    [self presentViewController:self.browserVC animated:YES completion:nil];
}

- (IBAction)disconnectButtonTapped:(UIButton *)sender { // Disconnect device when button tapped
    [self setUIToNotConnectedState];
    connected = NO;
    [self.session disconnect];
    self.statusLabel.text = @"Status: disconnected";
}

- (void)session:(MCSession *)session peer:(MCPeerID *)peerID didChangeState:(MCSessionState)state
{
    NSString *str = [NSString stringWithFormat:@"Status: %@", peerID.displayName]; // Display status if device is connected to another or not
    if (state == MCSessionStateConnected)
    {
        self.statusLabel.text = [str stringByAppendingString:@" connected"];
        [self setUIToConnectedState];
        connected = YES;
    }
    else if (state == MCSessionStateNotConnected)
    {
        self.statusLabel.text = [str stringByAppendingString:@" not connected"];
        [self setUIToNotConnectedState];
        connected = NO;
    }
}

- (void)session:(MCSession *)session didReceiveData:(NSData *)data fromPeer:(MCPeerID *)peerID
{
  
}

- (IBAction)StartButtonTapped:(UIButton *)sender {
    
    NSString *accX = _acc_X.text; // Get the value of each label for sending
    NSString *accY = _acc_Y.text;
    NSString *accZ = _acc_Z.text;
    
    NSString *gyX_2 = _gyX.text;
    NSString *gyY_2 = _gyY.text;
    NSString *gyZ_2 = _gyZ.text;
    
    NSArray *peerIDs = self.session.connectedPeers;
    
    [self.session sendData:[accX dataUsingEncoding:NSASCIIStringEncoding] // Send the data of each label
                   toPeers:peerIDs
                  withMode:MCSessionSendDataReliable error:nil];
    
    [self.session sendData:[accY dataUsingEncoding:NSASCIIStringEncoding]
                   toPeers:peerIDs
                  withMode:MCSessionSendDataReliable error:nil];
    
    [self.session sendData:[accZ dataUsingEncoding:NSASCIIStringEncoding]
                   toPeers:peerIDs
                  withMode:MCSessionSendDataReliable error:nil];
    
    [self.session sendData:[gyX_2 dataUsingEncoding:NSASCIIStringEncoding]
                   toPeers:peerIDs
                  withMode:MCSessionSendDataReliable error:nil];
    
    [self.session sendData:[gyY_2 dataUsingEncoding:NSASCIIStringEncoding]
                   toPeers:peerIDs
                  withMode:MCSessionSendDataReliable error:nil];
    
    [self.session sendData:[gyZ_2 dataUsingEncoding:NSASCIIStringEncoding]
                   toPeers:peerIDs
                  withMode:MCSessionSendDataReliable error:nil];}

// Received a byte stream from remote peer
- (void)session:(MCSession *)session didReceiveStream:(NSInputStream *)stream withName:(NSString *)streamName fromPeer:(MCPeerID *)peerID
{
    
}

// Start receiving a resource from remote peer
- (void)session:(MCSession *)session didStartReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID withProgress:(NSProgress *)progress
{
    
}

// Finished receiving a resource from remote peer and saved the content in a temporary location - the app is responsible for moving the file to a permanent location within its sandbox
- (void)session:(MCSession *)session didFinishReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID atURL:(NSURL *)localURL withError:(NSError *)error
{
    
}



- (void)browserViewControllerDidFinish:(MCBrowserViewController *)browserViewController
{
    [browserViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)browserViewControllerWasCancelled:(MCBrowserViewController *)browserViewController
{
    [browserViewController dismissViewControllerAnimated:YES completion:nil];
}



- (NSString *)participantID
{
    return self.session.myPeerID.displayName;
}



- (void)setUIToNotConnectedState
{
    
    self.disconnectButton.enabled = NO;
    self.browseButton.enabled = YES;
}

- (void)setUIToConnectedState
{
    self.disconnectButton.enabled = YES;
    self.browseButton.enabled = NO;
}-(void)startMonitoringMotion
{
    CMDeviceMotion *deviceMotion = self.motionManager.deviceMotion;
    
    if(deviceMotion == nil) {   return; }
    
    CMAttitude *attitude = deviceMotion.attitude;
    CMAcceleration userAcceleration = deviceMotion.userAcceleration;
    
    float roll = attitude.roll;
    
    float pitch = attitude.pitch;
    
    float yaw = attitude.yaw;
    
    float accX = userAcceleration.x;
    
    float accY = userAcceleration.y;
    
    float accZ = userAcceleration.z;
    
    [self updateLabels:roll Pitch:pitch Yaw:yaw accX:accX accY:accY accZ:accZ];
}



-(void)viewWillDisappear:(BOOL)animated
{
    
    [super viewWillDisappear:animated];
    
    if (self.motionManager !=nil)
    {
        [self.motionManager stopDeviceMotionUpdates];
        self.motionManager = nil;
    }
    
}





@end
